<?php
add_action( 'wp_enqueue_scripts', 'plop_enqueue_my_scripts' );

function plop_enqueue_my_scripts() {

	$parenthandle = 'twentytwenty-style'; // This is 'twentyfifteen-style' for the Twenty Fifteen theme.
	$theme        = wp_get_theme();

	wp_enqueue_style( $parenthandle,
		get_template_directory_uri() . '/style.css',
		array(),  // If the parent theme code has a dependency, copy it to here.
		$theme->parent()->get( 'Version' )
	);

	wp_enqueue_style(
        'plop-style',
		get_stylesheet_uri(),
		array( $parenthandle ),
		$theme->get( 'Version' ) // This only works if you have Version defined in the style header.
	);

}

function plop_register_post_type_project() {

	register_post_type(
		'project',
		array(
			'labels'				=> array(
				'name'					=> 'Projets',
				'singular_name'			=> 'Projet',
			),
			'public'				=> true,    // false = cachée de l'interface d'admin et du frontend
			'publicly_queryable'	=> true,    // Visible côté frontend ?
			'show_in_rest'			=> true,	// Nécessaire pour fonctionner avec Gutenberg
			'hierarchical'			=> false,
			'supports'				=> array( 'title', 'editor', 'thumbnail' ),
			'has_archive'			=> 'projets',
			'rewrite'				=> array( 'slug' => 'projet' ),
		)
	);

}
add_action('init', 'plop_register_post_type_project', 10);

function plop_register_ctx_project_type() {

	register_taxonomy(
		'project-type',
		'project',
		array(
			'labels'				=> array(
				'name'					=> 'Catégories de projet',
				'singular_name'			=> 'Catégorie de projet'
			),
			'public'				=> true,    // false = cachée de l'interface d'admin et du frontend
			'publicly_queryable'	=> true,    // Visible côté frontend ?
			'hierarchical'			=> true,   	// Fonctionne comme les catégories (true) ou comme les étiquettes (false)
			'show_in_rest'			=> true,	// Nécessaire pour fonctionner avec Gutenberg
			'rewrite'				=> array( 'slug' => 'categories-de-projets' ),
		)
	);

}

add_action( 'init', 'plop_register_ctx_project_type', 11 );

function plop_add_meta_boxes_project( $post ) {

	add_meta_box(
		'plop_mbox_project',                // Unique ID
		'Infos complémentaires du projet',  // Box title
		'plop_mbox_project_content', 		// Content callback, must be of type callable
		'project'                          	// Post type
	);

}

add_action( 'add_meta_boxes', 'plop_add_meta_boxes_project' );

function plop_mbox_project_content( $post ) {

	// Get meta value
	$plop_year = get_post_meta(
		$post->ID,
		'plop-year',
		true
	);
	$plop_email = get_post_meta(
		$post->ID,
		'plop-email',
		true
	);

	echo '<p>';
	echo '<label for="plop-year">';
	echo 'Date de création';
	echo '<input type="number" id="plop-year" name="plop-year" value="' . $plop_year . '">';
	echo '</label>';
	echo '</p>';

	echo '<p>';
	echo '<label for="plop-email">';
	echo 'Email de contact';
	echo '<input type="email" id="plop-email" name="plop-email" value="' . $plop_email . '">';
	echo '</label>';
	echo '</p>';

}

// Save post meta
function plop_save_post( $post_id ) {

	if ( isset( $_POST['plop-year'] ) && !empty( $_POST['plop-year'] ) ) {

		update_post_meta(
			$post_id,
			'plop-year',
			sanitize_text_field( $_POST['plop-year'] )
		);

		update_post_meta(
			$post_id,
			'plop-email',
			sanitize_email( $_POST['plop-email'] )
		);

	}

}

add_action( 'save_post', 'plop_save_post' );

